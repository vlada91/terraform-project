
resource "aws_eks_cluster" "eks_cluster" {

  depends_on = [aws_iam_role_policy_attachment.eks_iam_policy_attachment]

  name     = "my_cluster"
  role_arn = aws_iam_role.eks_iam_role.arn

  vpc_config {
    subnet_ids         = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id, aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]
    security_group_ids = [aws_security_group.main_sg_group.id]
  }

}
