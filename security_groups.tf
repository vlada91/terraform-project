resource "aws_security_group" "main_sg_group" {
  name        = "main-security-group"
  description = "main security group"

  vpc_id = aws_vpc.kubernetes_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }
}

