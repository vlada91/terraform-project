resource "aws_eks_node_group" "my_node_group" {

  depends_on = [
    aws_iam_role_policy_attachment.worker_node_policy_attachment,
    aws_iam_role_policy_attachment.cni_policy_attachment,
    aws_iam_role_policy_attachment.ec2_container_registry_read_only,
    aws_eks_cluster.eks_cluster
  ]

  cluster_name    = "my_cluster"
  node_group_name = "my-node-group"
  node_role_arn   = aws_iam_role.eks_node_role.arn

  subnet_ids = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]

  ami_type       = "AL2_x86_64"
  instance_types = ["m5.large"]
  disk_size      = 20

  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 1
  }

}
