# Task 1 - Terraform

- Hashicorp needed to be installed to use Terraform.

- AWS CLI 2 and Kubectl must be installed and profile must be set up.

- AWS IAM user with name terraform-user is created on AWS with privileges to create resources on the cloud.

- In order for code to be more readable and maintainable, almost every cloud resource was  written into a separate Terraform file. So, for example, VPC has its own file, subnets have their own files etc… 

- I have made two private and two public subnets in VPC. When using AWS services like rds, it is recommended to put for example MySQL server into two different subnets to make it more resilient and robust.

- Routing table was made for both private and public subnets. Routing table for the public subnet is connected to the internet gateway so there could be traffic from the outside.

- Two IAM roles had to be created in order for the EKS cluster to be made and for worker nodes to operate. Those roles and all needed privileges are in a separate iam.tf file.

- For EC2 instances that are part of node group configuration is next:

  ami_type   	= "AL2_x86_64"
  instance_types = ["m5.large"]
  disk_size  	= 20

- For AMI type, Amazon Linux 2 OS is selected, it is most commonly used.
  Because this is not a real production application, a smaller EC2 instance of type m5.large was  used.

  -All Terraform scripts needed to make AWS infrastructure are inside the Terraform-project folder.

- To make AWS cloud infrastructure, do next commands:

          terraform init
          terraform fmt  
          terraform validate
          terraform plan
          terraform apply

   In order for load balancer to be created, the nginx ingress controller file is deployed after infrastructure has been created.
   Nginx ingress file could also be deployed via helm installer.

- To test connection to cluster and to configure kubectl use command:
  aws eks update-kubeconfig --region eu-central-1 --name my_cluster   
        

- To destroy AWS cloud infrastructure, commands are:

          Load Balancer was created by applying nginx-ingress-controller file, not with Terraform, so it must be deleted from AWS GUI.
          Security groups created automatically with load balancer must be deleted too before running terraform destroy command.

          terraform destroy

