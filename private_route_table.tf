resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.kubernetes_vpc.id
  tags = {
    Name = "private_rt"
  }
}

