resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.kubernetes_vpc.id
  tags = {
    Name = "igw"
  }
}
